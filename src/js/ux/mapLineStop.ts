import * as L from 'leaflet';
import * as mapManager from "./mapmanager"
import { getStopsData } from "../localStorageManager/stops";
import { getCodeLineTrace } from "../localStorageManager/lines"

var map: L.Map;

const url:URL = new URL(window.location.href);
const currentSearchParams:URLSearchParams = new URLSearchParams(url.search);
const lineCode: string = currentSearchParams.get("code_ligne");
const nameStops: Array<string> = currentSearchParams.getAll("name_stop");

//console.log(lineCode)
//console.log(nameStops)

function displayStops() {
    nameStops.forEach(async nameStop => {
        const stopData = await getStopsData(nameStop)
        //stopData.records[0].geo_point_2d
        //console.log(stopData.records[0].fields.geo_point_2d)
        var marker = L.marker(stopData.records[0].fields.geo_point_2d,{icon:mapManager.getStopIcon()}).bindPopup(nameStop).addTo(map); 
    });
}

async function displayTraceLine(){
    const linePath = await getCodeLineTrace(lineCode);

    var line = new L.Polyline(linePath, {color: 'red'});
    line.addTo(map);
}

async function main() {
    map = await mapManager.initMap();

    displayStops();
    displayTraceLine();
}

main();