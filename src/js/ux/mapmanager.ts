import * as L from 'leaflet';
import 'leaflet/dist/leaflet.css';

export function initMap(): Promise<L.Map> {
    return new Promise((resolve) => {
        document.addEventListener("DOMContentLoaded", () => {
            const map: L.Map = L.map('map').setView([45.564601, 5.917781], 13);

            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
            }).addTo(map);

            resolve(map);
        })
    })
}

export function getBusIcon(): L.Icon {
    
    return ;
}

export function getStopIcon(): L.Icon {
   return L.icon({
        iconUrl: 'https://img.icons8.com/material-sharp/24/000000/cloud-network.png',
        
    });
}

export function getTerminusIcon(): L.Icon {
    //TODO
    return ;
}
