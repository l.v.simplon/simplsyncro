export type LineNameCodeMap = {
    lineCode: string, lineName: string
};

type Coordinates = [number, number];


//TODO: use @types/geojson types
type GeoShape = {
  type: string,
  coordinates: any //TODO
}

interface GeoShapePoint extends GeoShape {
  type: "Point",
  coordinates: Coordinates
}

export type LineString = Array<Coordinates>;

interface GeoShapeMultiPoint extends GeoShape {
  type: "MultiPoint",
  coordinates: LineString
}


interface GeoShapeMultiLineString extends GeoShape {
  type: "MultiLineString",
  coordinates: Array<LineString>
}

interface GeoShapeMultiPoint extends GeoShape {
  type: "MultiPoint",
  coordinates: Array<Coordinates>
}

type ApiTronconShape = {
  datasetid: string,
  recordid: string,
  fields: {
    lignes: string,
    commune: string,
    nom_arret: string,
    geo_shape: GeoShapeMultiPoint,
    geo_point_2d: Coordinates
  },
  geometry: GeoShapePoint,
  record_timestamp: string
}

type Facet = {
  count: number,
  path: string,
  state: string,
  name: string
};

type FacetGroup = {
  facets: Array<Facet>,
  name: string
};

type ApiResponseParameters = {
  dataset: string,
  timezone: string,
  rows: number,
  format: string,
  facet: Array<string>
}
export type ApiTronconShapeResponse = {
  nhits: number,
  parameters: ApiResponseParameters,
  records: Array<ApiTronconShape>,
  facet_groups: Array<FacetGroup>
};

type ApiTronconLineRecord = {
  datasetid: string,
  recordid:string,
  fields:{
    commune:string,
    nom_ligne: string,
    geo_point_2d:Coordinates,
    code_ligne:string,
    geo_shape: GeoShapeMultiLineString,
    arret_dess: string
  },
  geometry: GeoShapePoint,
  record_timestamp: string,
}

export type ApiTronconLineResponse = {
  nhits: number,
  parameters:ApiResponseParameters,
  records: Array<ApiTronconLineRecord>,
  facet_groups: Array<FacetGroup>
}

