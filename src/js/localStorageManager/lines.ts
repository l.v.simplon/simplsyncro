import * as stopsAPI from "../api/stops"
import * as linesAPI from "../api/lines"
import { storageAvailable } from "../utils"
import { LineString } from "../types";

async function fetchAndFilterStops(lineCode: string) {

    const stopsData = await stopsAPI.searchStopsForLine(lineCode)

    const filteredRecords = stopsData.records.filter(record => {
        const lineNames = record.fields.lignes.split(',');
        console.log(lineNames)

        return lineNames.includes(lineCode)
    });

    return filteredRecords;
}

async function fetchStopsAndGetNames(lineCode: string) {
    const stops = await fetchAndFilterStops(lineCode);
    const stopNames = stops.map(stop => stop.fields.nom_arret)

    return stopNames;
}

export async function getStopNamesForLine(lineCode: string) {
    if (storageAvailable('localStorage')) {
        const dataInStorage = localStorage.getItem(lineCode)
        if (dataInStorage === null) {
            const stopNames = await fetchStopsAndGetNames(lineCode);
            localStorage.setItem(lineCode, JSON.stringify(stopNames));
            return stopNames;  
        }
        else {
            return <string[]> JSON.parse(dataInStorage)
        }
    }
    else {
        return fetchStopsAndGetNames(lineCode);
    };
}

export async function getCodeLineTrace(lineCode: string): Promise<LineString[]> {
    if (storageAvailable('localStorage')) {
        const key = `trace-${lineCode}`;
        const dataInStorage = localStorage.getItem(key)

        if (dataInStorage === null) {
            const stopNames = await linesAPI.getCodeLineTrace(lineCode);
            localStorage.setItem(key, JSON.stringify(stopNames));
            return stopNames;  
        }
        else {
            return JSON.parse(dataInStorage)
        }
    }
    else {
        return linesAPI.getCodeLineTrace(lineCode);
    };
}

