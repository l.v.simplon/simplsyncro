import * as constants from "../constants";
import { getStopNamesForLine } from "../localStorageManager/lines";
const pickedStops: Set<string> = new Set();
var button: HTMLSelectElement;
const url:URL = new URL(window.location.href);
const currentSearchParams:URLSearchParams = new URLSearchParams(url.search);
export const lineCode: string = currentSearchParams.get("code_ligne");

console.log("querying api for stops for line", lineCode);

function createLineElem(stopName: string) : string {
  return `
    <article data-stop-name="${stopName}" class="stop my-2 bg-dark text-light border rounded-top rounded-bottom border-dark">
      <p class ="mt-3" style="pointer-events: none">
          ${stopName}
      </p>
    </article>
  `;
  
}

document.addEventListener("DOMContentLoaded", async () => {
  const stopNames = await getStopNamesForLine(lineCode);
  button = <HTMLSelectElement>document.getElementById('valideStop')
  stopNames.forEach(stopName => {
    document.getElementById('choiceStop').innerHTML += createLineElem(stopName)
  });

  const stopElmCol = document.getElementsByClassName('stop')
  const stopElms = Array.from(stopElmCol)

  stopElms.forEach(element => {
    element.addEventListener('click', (e) => {
      const elem = <HTMLElement>e.target;
      const stopName = elem.dataset.stopName;
      
      if (pickedStops.has(stopName)){
        pickedStops.delete(stopName)
        elem.classList.remove("bg-success")
        elem.classList.remove("text-dark");
        elem.classList.add("bg-dark")
        elem.classList.add("text-light")
      }else{
        pickedStops.add(stopName)
        elem.classList.remove("bg-dark")
        elem.classList.remove("text-light");
        elem.classList.add("bg-success")
        elem.classList.add("text-dark");
      };

      toggleButton()
      
      const newUrlParams = new URLSearchParams();
      newUrlParams.set("code_ligne", lineCode);

      pickedStops.forEach(stopName => {
        newUrlParams.append("name_stop", stopName);        
      });

      const link = <HTMLLinkElement>button.parentElement;
      link.href = "map.html?" + newUrlParams.toString();

      console.log(pickedStops)
    })
  }); 
})



function toggleButton(){ 
  if ( pickedStops.size >= 2){
    button.disabled = false;
  }else{
    button.disabled = true;
  }
}


